<?php
spl_autoload_register(function ($nombreClase) {
    require_once "$nombreClase.php";
});

use clases\elementos\Moto;
use clases\librerias\Conexion;
use clases\librerias\Utilidades;


// Cargamos el menu
require_once '_menu.php';

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="estilos/main.css">
    <script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.js"></script>
</head>

<body>

    <?php

    $conexion = new Conexion([
        'baseDatos' => 'concesionarioMotos',
    ]);

    // Datos del coche en un array
    $datos = $conexion
        ->consulta("
        SELECT * FROM motos WHERE id ='{$_GET["id"]}'
        ")
        ->obtenerDatos()[0];



    // Datos del coche en un objeto
    $modelo = new Moto([
        'id' => $datos['id'],
        'marca' => $datos['marca'],
        'modelo' => $datos['modelo'],
        'precio' => $datos['precio'],
        'matricula' => $datos['matricula'],
    ]);

    ?>
    <div class="mostrarDatosVer">
        <?php
        echo $modelo->dibujarTabla();
        ?>
    </div>
    <div class="botoneraVer">
        <div class="botonesVer"><a href="actualizar.php?id=<?= $_GET["id"] ?>" class="iconosGridUpdate"> <span>Editar <ion-icon name="pencil-outline"></ion-icon></span></a></div>
        <div class="botonesVer"><a href="eliminar.php?id=<?= $_GET["id"] ?>" class="iconosGridDelete"> <span>Borrar <ion-icon name="trash-outline"></ion-icon></span></a></div>
    </div>
</body>

</html>