﻿DROP DATABASE IF EXISTS concesionarioMotos;

CREATE DATABASE concesionarioMotos;

USE concesionarioMotos;

CREATE TABLE motos(
id int,
matricula varchar(15),
precio float ,
marca varchar(100),
modelo varchar (100),
PRIMARY KEY (id),
UNIQUE KEY (matricula)
);


INSERT INTO motos VALUES  
 ( 1, '5621HHH', 1236 , 'Honda', 'GT52697');