<?php

namespace clases\elementos;

class Moto
{
    public  $marca;
    public  $modelo;
    public  $id;
    public  $matricula;
    public $precio;
    private $idVieja; // para la variable de sesion


    /**
     * __construct
     *
     * @param  array $datos debes pasar un array asociativo con las propiedades a inicializar
     * son marca, modelo, id, matricula y precio
     * @return void
     */
    public function __construct($datos = [])
    {
        $this->marca = $datos['marca'] ?? '';
        $this->modelo = $datos['modelo'] ?? '';
        $this->id = $datos['id'] ?? null;
        $this->matricula = $datos['matricula'] ?? '';
        $this->precio = $datos['precio'] ?? 0;
        $this->idVieja = $datos['idVieja'] ?? '';
    }

    public function __toString()
    {
        $salida = "<ul>";
        $salida .= "<li> ID: {$this->id} </li>";
        $salida .= "<li> Matricula: {$this->matricula} </li>";
        $salida .= "<li> Marca: {$this->marca} </li>";
        $salida .= "<li> Modelo: {$this->modelo} </li>";
        $salida .= "<li> Precio: {$this->precio} </li>";
        $salida .= "</ul>";
        return $salida;
    }

    // GETTERS
    public function getMarca()
    {
        return $this->marca;
    }

    public function getModelo()
    {
        return $this->modelo;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getMatricula()
    {
        return $this->matricula;
    }

    public function getPrecio()
    {
        return $this->precio;
    }

    // SETTERS
    public function setMarca($marca)
    {
        $this->marca = $marca;

        return $this;
    }

    public function setModelo($modelo)
    {
        $this->modelo = $modelo;

        return $this;
    }

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function setMatricula($matricula)
    {
        $this->matricula = $matricula;

        return $this;
    }

    public function setPrecio($precio)
    {
        $this->precio = $precio;

        return $this;
    }

    public function dibujarTabla()
    {
        ob_start();
?>
        <table class="contenidoTablas" style="text-align: center; border-collapse: collapse;">
            <thead>
                <tr>
                    <td>ID</td>
                    <td>Matricula</td>
                    <td>Marca</td>
                    <td>Modelo</td>
                    <td>Precio</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td> <?= $this->getId() ?></td>
                    <td> <?= $this->getMatricula() ?></td>
                    <td> <?= $this->getMarca() ?></td>
                    <td> <?= $this->getModelo() ?></td>
                    <td> <?= $this->getPrecio() ?></td>
                </tr>
            </tbody>
        </table>
<?php
        return ob_get_clean();
    }

    public function insertar($conexion)
    {
        $conexion->consulta("INSERT INTO motos VALUES ( {$this->id}, '{$this->matricula}', {$this->precio} , '{$this->marca}', '{$this->modelo}')");
    }

    public function actualizar($conexion)
    {
        $conexion->consulta("UPDATE motos SET marca = '{$this->marca}', modelo = '{$this->modelo}', precio = {$this->precio}, matricula = '{$this->matricula}', id= {$this->id} WHERE id={$this->idVieja}");
    }

    public function eliminar($conexion)
    {
        $conexion->consulta("DELETE FROM motos WHERE id='{$this->id}'");
    }
}
