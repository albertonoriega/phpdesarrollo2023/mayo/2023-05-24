<form method="get">
    <div>
        <p>Introduce los datos de la nueva moto</p>
    </div>
    <div>
        <label for="id">ID</label>
        <input type="text" name="id" class="id" value="<?= $modelo->id ?>" required>
    </div>
    <div>
        <label for=" matricula">Matricula</label>
        <input type="text" name="matricula" class="matricula" value="<?= $modelo->matricula ?>" required>
    </div>
    <div>
        <label for=" precio">Precio</label>
        <input type="number" name="precio" class="precio" value="<?= $modelo->precio ?>" required>
    </div>
    <div>
        <label for=" marca">Marca</label>
        <input type="text" name="marca" class="marca" value="<?= $modelo->marca ?>" required>
    </div>
    <div>
        <label for=" modelo">Modelo</label>
        <input type="text" class="modelo" name="modelo" value="<?= $modelo->modelo ?>" required>
    </div>
    <div>
        <button class=" botonForm" name="enviar">Enviar</button>
        <button class="botonForm" type="reset" name="botonLimpiar">Limpiar</button>
    </div>

</form>