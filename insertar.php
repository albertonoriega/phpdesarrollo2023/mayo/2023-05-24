<?php

spl_autoload_register(function ($nombreClase) {
    require_once "$nombreClase.php";
});

use clases\elementos\Moto;
use clases\librerias\Conexion;

$conexion = new Conexion([
    'baseDatos' => 'concesionarioMotos',
]);

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="estilos/main.css">
</head>

<body>


    <?php

    if (isset($_GET['enviar'])) {

        extract($_GET, EXTR_PREFIX_SAME, "form");


        // Ya tenemos un metodo en la clase Moto para insertar en la BBDD
        // Instanciamos un objeto
        $modelo = new Moto([
            'id' => $id,
            'marca' => $marca,
            'modelo' => $modelo,
            'precio' => $precio,
            'matricula' => $matricula
        ]);
        // Lo insertamos en la BBDD
        $modelo->insertar($conexion);

        //redireccionar la página a index
        header('Location: index.php');
    } else {
        // Cargamos el menu
        require_once '_menu.php';

        $modelo = new Moto();

        // Cargamos el formulario
        require_once '_form.php';
    }

    ?>
</body>

</html>