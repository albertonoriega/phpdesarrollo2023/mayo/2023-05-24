<?php
spl_autoload_register(function ($nombreClase) {
    require_once "$nombreClase.php";
});

use clases\elementos\Moto;
use clases\librerias\Conexion;

session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="estilos/main.css">
</head>

<body>
    <?php
    $conexion = new Conexion([
        'baseDatos' => 'concesionarioMotos',
    ]);

    // Cuando se pulsa eliminar y tenemos id por la URL
    if (isset($_GET['id'])) {
        // Generamos una variable donde guardamos los campos del bastidor que pasamos
        $datos = $conexion->consulta("SELECT * FROM motos WHERE id= '{$_GET["id"]}' ")->obtenerDatos()[0];

        // Generamos un objeto con los datos
        $modelo = new Moto([
            'id' => $datos['id'],
            'marca' => $datos['marca'],
            'modelo' => $datos['modelo'],
            'precio' => $datos['precio'],
            'matricula' => $datos['matricula'],
        ]);

        $_SESSION['id'] = $_GET['id'];
        require_once '_ver.php';

        // Si se ha pulsado el boton eliminar de la subvista ver
    } elseif (isset($_GET['eliminar'])) {

        $conexion = new Conexion([
            'baseDatos' => 'concesionarioMotos',
        ]);
        // Generamos una variable donde guardamos los campos del bastidor que pasamos
        $datos = $conexion->consulta("SELECT * FROM motos WHERE id= '{$_SESSION["id"]}' ")->obtenerDatos()[0];

        // Generamos un objeto con los datos
        $modelo = new Moto([
            'id' => $datos['id'],
            'marca' => $datos['marca'],
            'modelo' => $datos['modelo'],
            'matricula' => $datos['matricula'],
            'precio' => $datos['precio'],
        ]);

        $modelo->eliminar($conexion);

    ?>
        <div class="botonesVer">
            <p>El registro se ha eliminado</p>
            <a href="index.php">Volver al inicio</a>
        </div>
    <?php
    } else {
        echo "No tengo los datos de la moto a eliminar";
    }
    ?>
</body>

</html>