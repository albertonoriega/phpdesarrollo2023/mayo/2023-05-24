<?php

spl_autoload_register(function ($nombreClase) {
    require_once "$nombreClase.php";
});

// Para que funcionen las sesiones
session_start();

use clases\elementos\Moto;
use clases\librerias\Conexion;

// Cargamos el menu
require_once '_menu.php';

$conexion = new Conexion([
    'baseDatos' => 'concesionarioMotos',
]);

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="estilos/main.css">
</head>

<body>
    <?php

    // Si se ha pulsado el boton enviar del formulario
    if (isset($_GET['enviar'])) {
        $modelo = new Moto([
            'id' => $_GET['id'],
            'marca' => $_GET['marca'],
            'modelo' => $_GET['modelo'],
            'precio' => $_GET['precio'],
            'matricula' => $_GET['matricula'],
            'idVieja' => $_SESSION['id'], // bastidor que está en la base de datos y que se va a cambiar
        ]);
        $modelo->actualizar($conexion);

        header("Location: index.php");
    } else {
        // Guardo el bastidor en una variable de sesion
        $_SESSION['id'] = $_GET['id'];

        // Consulta para sacar los datos de un registro filtrando por el bastidor
        $datos = $conexion
            ->consulta("
        SELECT * FROM motos WHERE id = {$_SESSION["id"]}
        ")
            ->obtenerDatos();

        $modelo = new Moto([
            'id' => $datos[0]['id'],
            'matricula' => $datos[0]['matricula'],
            'marca' => $datos[0]['marca'],
            'modelo' => $datos[0]['modelo'],
            'precio' => $datos[0]['precio'],
        ]);

        require_once '_form.php';
    }
    ?>
</body>

</html>